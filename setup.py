#!/usr/bin/env python
from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

with open("cgeweb/__init__.py", 'r') as f:
    for l in f:
        if l.startswith('__version__'):
            version = l.split('=')[1].strip().strip('"')

setup(
   name='cgeweb',
   version=version,
   description='Center for Genomic Epidemiology Web Server Module',
   long_description=long_description,
   license="Apache License, Version 2.0",
   author='Martin Thomsen',
   author_email='martt@dtu.dk',
   url="https://bitbucket.org/genomicepidemiology/cge_web_module",
   packages=['cgeweb']
)