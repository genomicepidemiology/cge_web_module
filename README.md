# cge_web_module

Web server module for the Center for Genomic Epidemiology

This module contains classes and functions related to the CGE web services

# How to update:
1. Make changes to the modules
2. Bump the version number accordingly in cgeweb/__init__.py
3. Install package locally
4. Test the changes locally (for both python2 and python3)
5. Distribute to Pypi

# Install package locally
python2 setup.py install
python3 setup.py install

# Distribute to PyPi
python setup.py sdist upload -r pypi
