def tsv2html(fp, full_dl='', css_classes='greyhead'):
   printDebug('Writing HTML table for %s'%fp)
   if os.path.exists(fp):
      with open(fp, 'r') as f:
         intable = False
         tsize = 0
         ttitle = ''
         for l in f:
            l = l.strip()
            if l == '':
               if intable:
                  # END TABLE
                  printOut("</table>\n")
                  intable = False
            elif l[0] == '#':
               # HEADER LINE
               tmp = l[1:].split('\t')
               if intable: pass
                  ## Print header line
                  #tsize = len(tmp)
                  #printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
               elif len(tmp) > 1:
                  # Start Table
                  tsize = len(tmp)
                  printOut("<table class='results %s'>\n"%(css_classes))
                  # Print Title Line
                  if ttitle != '':
                     printOut("<tr><th colspan='%s'>%s\n"%(tsize, ttitle))
                     if full_dl != '': dlButton('View All', full_dl+'.gz')
                     printOut("</th></tr>\n")
                  # Print Header Line
                  printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                  intable = True
               else:
                  # Create new table
                  ttitle = tmp[0]
                  #printOut("<table class='results %s'>\n"%(css_classes))
                  #printOut("<tr><th><H2>%s:</H2></td><td>\n"%(name))
                  #if full_dl != '': dlButton('View All', full_dl+'.gz')
                  #printOut("</td></tr></table><br>\n")
                  #intable = True
            elif intable:
               # print table row
               td = l.split('\t')
               tdl = len(td)
               printOut("<tr><td>%s</td></tr>\n"%('</td><td>'.join([str(td[i]) if i < tdl else '&nbsp;' for i in xrange(tsize)])))
         # END TABLE IF OPEN
         if intable: printOut("</table>\n")

def ext_out2html(fp, nuc_link=False, aln_start=20, css_classes='',twidth='85%'):
   ''' Convert CGEs extended output file format to HTML
   
   ARGS:
      * aln_start sets the starting position of the alignments. A 20 chars header is
        asumed by default.
      * if nuc_link is True, the last entry in the table will be a link to
        www.ncbi.nlm.nih.gov/nuccore/
   '''
   if os.path.exists(fp):
      with open(fp, 'r') as f:
         in_table = False
         in_ext = False
         in_note = False
         for l in f:
            if in_ext:
               if l.strip() == '': continue
               if l.startswith('-----'):
                  # Entry split
                  # printOut("\n\n")
                  prev_color = 'grey'
                  a_buff = []
                  q_buff = []
               else:
                  tmp = l.strip().split(':')
                  if len(tmp) > 2:
                     # Print entry header
                     printOut("<h3><i>%s</i>:%s:<i>%s</i>-%s</h3>"%tuple(
                        [tmp[0], ':'.join(tmp[1:])] + re.split('[\-_]',tmp[-1])
                        ))
                     aln_len, allele_len = map(int,l.split(',')[2].split(':')[1].strip().split('/'))
                     cur_len = 0
                  else:
                     # prev_color = 'grey'
                     # a_buff = []
                     # q_buff = []
                     # aln_start = 20
                     # cur_len = 0
                     # aln_len = 39
                     # txt = '''pMLST allele seq:     acagctggcgtaaaccacttccggcaagaataaccatatgacgaatagcagcaggata
                     #                        |||||||||||||||||||| |||||||||||||||||||||||||||||||||
                     # Hit in genome:      aggcagctggcgtaaaccacttcgggcaagaataaccatatgacgaatagcagcaggc
                     # '''.split('\n')
                     # txt = '''pMLST allele seq:   acagtgaggaggcttttgcggttgagttcgccagtgataccgg
                     #                     |||||||||||||||||||||||||||||||||||||||
                     # Hit in genome:      acagtgaggaggcttttgcggttgagttcgccagtgatatcgg
                     # '''.split('\n')
                     # 
                     # a_line = txt[0]
                     # m_line = txt[1]
                     # q_line = txt[2]
                     a_line = l
                     m_line = f.readline()
                     q_line = f.readline()
                     allele_txt = a_line[:aln_start].strip().rstrip(':')
                     allele = a_line[aln_start:].rstrip()
                     a_length = len(allele)
                     matches = m_line[aln_start:].rstrip()
                     query_txt = q_line[:aln_start].strip().rstrip(':')
                     query = q_line[aln_start:].rstrip()
                     q_length = len(query)
                     # Add title
                     a_buff.append("<span>%s</span>"%(allele_txt.ljust(aln_start)))
                     q_buff.append("<span>%s</span>"%(query_txt.ljust(aln_start)))
                     # Parse alignment
                     min_length = a_length if a_length < q_length else q_length
                     max_length = a_length if a_length > q_length else q_length
                     matches = matches.ljust(max_length)
                     cur_len += max_length
                     if cur_len > aln_len:
                        min_length -= cur_len - aln_len
                     
                     i = 0
                     in_match = False
                     if prev_color == 'green':
                        in_match = False
                     
                     while i < min_length:
                        # Select span color
                        is_match = matches[i] == '|'
                        if is_match:
                           color = 'green'
                        elif prev_color == 'grey':
                           color = 'grey'
                           prev_color = 'red'
                        else:
                           color = 'red'
                        a_buff.append("<span class='bg%s'>"%color)
                        q_buff.append("<span class='bg%s'>"%color)
                        # Find span length
                        while i < min_length:
                           if matches[i] != '|':
                              if is_match and matches[i] != '|':
                                 end = i
                                 break
                           else:
                              if not is_match:
                                 end = i
                                 break
                           a_buff.append(allele[i])
                           q_buff.append(query[i])
                           i+=1
                        
                        a_buff.append("</span>")
                        q_buff.append("</span>")
                        if color == 'grey':
                           cur_len -= i
                     
                     if i < max_length:
                        # Handle trailing end
                        prev_color = 'grey'
                        color = 'grey'
                        a_buff.append("<span class='bg%s'>"%color)
                        q_buff.append("<span class='bg%s'>"%color)
                        while i < max_length:
                           a_buff.append(allele[i] if i < a_length else ' ')
                           q_buff.append(query[i] if i < q_length else ' ')
                           i+=1
                        
                        a_buff.append("</span>")
                        q_buff.append("</span>")
                     else:
                        prev_color = color
                     
                     printOut("%s\n%s\n"%(''.join(a_buff), ''.join(q_buff)))
                     a_buff = []
                     q_buff = []
                     # print(cur_len)
            elif in_table:
               if l.strip() == '': continue
               tmp = re.split('  +',l.strip())
               if len(tmp) > 1:
                  if nohead:
                     # tsize = len(tmp)
                     nohead = False
                     printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                  else:
                     # Select row color
                     p_id = tmp[1] == '100.00'
                     p_cov = tmp[2] == tmp[3]
                     color = 'bgred'
                     if p_id and p_cov:
                        color = 'bggreen'
                     elif not p_id and p_cov:
                        color = 'bglightgreen'
                     elif p_id and not p_cov:
                        color = 'bggrey'
                     # Add link to match
                     if nuc_link:
                        tmp[-1] = "<a href='http://www.ncbi.nlm.nih.gov/nuccore/%s'>%s</a>"%(tmp[-1], tmp[-1])
                     printOut("<tr class='%s'><td>%s</td></tr>\n"%(color,"</td><td>".join(tmp)))
               elif l.startswith('*****'):
                  pass
               else:
                  # END TABLE
                  printOut("</table>\n")
                  in_table = False
            elif l.startswith('*****'):
               # Start Table
               in_table = True
               nohead = True
               # printOut("<table class='center virresults' width='85%'>\n")
               printOut("<table class='results %s' width='%s'>\n"%(css_classes, twidth))
            elif l.startswith('Extended Output:'):
               in_ext = True
               # Print extended output button
               printOut("<script type='text/javascript'>"
                        "function printOutput(tag){"
                        "var ele = document.getElementById(tag).style;"
                        "if (ele.display=='block'){ele.display='none';}"
                        "else{ele.display='block';}}</script>\n")
               printOut("<center><button type='button' "
                        "onclick='printOutput(&quot;eo&quot;)'>"
                        "extended output</button></center>"
                        "<div id='eo' class='hide'>")
               printOut("<pre><br>")
            elif ':' in l:
               var, val = l.split(':')
               printOut("<h2>%s: &nbsp;<i class='grey'>%s</i></h2>"%(var.strip(), val.strip()))
            elif in_note:
               if l.strip() == '':
                  # End and print note
                  in_note = False
                  printOut("<div class='red' style='width:%s'>%s</div>\n\n"%(twidth, ' '.join(note)))
               else:
                  note.append(l.strip())
            elif l.startswith('* '):
               # Start note
               in_note = True
               note = [l.strip()[2:]]
   
   if in_ext:
      # End extended output
      printOut("</pre><br><br></div>")
